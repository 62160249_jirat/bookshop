/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.jirat.midtemexam;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ACER
 */
public class SalesService {

    private static ArrayList<Product> productList = new ArrayList<>();
    static {
        load();
    }

    //Create (C)
    public static boolean addProduct(Product item) {
        productList.add(item);
        refreshId();
        save();
        return true;
    }

    //Delete (D)
    public static boolean delProduct(Product item) {
        productList.remove(item);
        refreshId();
        save();
        return true;
    }

    public static boolean delProduct(int index) {
        productList.remove(index);
        refreshId();
        save();
        return true;
    }

    public static boolean delAllProduct() {
        productList.removeAll(productList);
        refreshId();
        save();
        return true;
    }

    //Read (R)
    public static ArrayList<Product> getProduct() {
        refreshId();
        save();
        return productList;
    }

    public static Product getProduct(int index) {
        refreshId();
        save();
        return productList.get(index);
    }

    //Update (U)
    public static boolean updateProduct(int index, Product item) {
        productList.set(index, item);
        refreshId();
        save();
        return true;
    }

    //Calculation
    public static String totalPrice() {
        double total = 0;
        for (int i = 0; i < productList.size(); i++) {
            total += productList.get(i).getPrice() * productList.get(i).getAmount();

        }
        return total + "";
    }

    public static String AmountTotal() {
        int amountotal = 0;
        for (int i = 0; i < productList.size(); i++) {
            amountotal += productList.get(i).getAmount();

        }
        return amountotal + "";
    }

    //RunIdAuto
    public static String refreshId() {
        if (!productList.isEmpty()) {
            int id = 0;
            for (int i = 0; i < productList.size(); i++) {
                productList.get(i).setId(i + 1);
                id = i + 2;
            }
            return Integer.toString(id);
        }
        return "1";
    }

    //File
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        trySave();

    }

    public static void trySave() {
        File file;
        FileOutputStream fos;
        ObjectOutputStream oos;
        try {
            file = new File("book.dat");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(productList);
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        tryLoad();

    }

    public static void tryLoad() {
        File file;
        FileInputStream fis;
        ObjectInputStream ois;
        try {
            file = new File("book.dat");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            productList = (ArrayList<Product>) ois.readObject();
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SalesService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
